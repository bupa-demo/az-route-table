azure-terraform-modules

# Introduction 
Azure terraform module to create an Azure route table and custom rules. Associate route with subnet

# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Provider
| Name    | Version |
| :---:   | :---:   |
| terraform | >=1.3.1 |
| azurerm | >=3.25.0 |

# Dependencies
This module depends on a new or existing resource group (refer to resource group module)

# Usage
Reference the module as below and pass required and/or optional arguments to provision a network security group. This will create a single Route table and  additional routes can be added as needed.

```
module "route-table" {
  source                        = "git@ssh.dev.azure.com:v3/Delu2011/az-terraform-module/az-route-table"
  route_table_name              = var.route_table_name
  location                      = module.vnet_resource_group.resource_group_location
  resource_group_name           = module.vnet_resource_group.resource_group_name
  disable_bgp_route_propagation = var.disable_bgp_route_propagation
  route_name                    = var.route_name
  enable_force_tunneling        = var.enable_force_tunneling
  tags                          = var.default_tags
}
```

Example vars
```
###### Route Table ######
route_table_name                = "RT-v1-0-0-0-1"
disable_bgp_route_propagation   = false
route_name = [
  {
    name                        = "RT-v1-0-0-0-1_117"
    address_prefix              = "10.1.0.0/16"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.1.0.3"
  },
  {
    name                        = "RT-v1-0-0-0-1_118"
    address_prefix              = "10.2.0.0/16"
    next_hop_type               = "VirtualAppliance"
    next_hop_in_ip_address      = "10.1.0.3"
  }
]
enable_force_tunneling          = true
```

# Inputs
| Name | Description | Type | Default | Required |
| :---: | :---: | :---: | :---: | :---: |
| route_table_name | Name of route table to be created | string | n/a | yes |
| location | Azure region to use | string | n/a | yes |
| environment | Environment to deploy | string | n/a | no | 
| project | Name of project | string | n/a | no | 
| resource_group_name | Resource group name | string | n/a | yes |
| disable_bgp_route_propagation | Disable BGP route propagation on this Route Table | bool | n/a | no |
| enable_force_tunneling | enable a route to Force Tunneling (force 0.0.0.0/0 traffic through the Gateway next hop) | bool | n/a | no | - Disabled
| route_name | Route table name | map(object) | n/a | no |
| tags | Tags to add to resources | map(string) | n/a | no |


# Outputs
| Name | Description |
| :---: | :---: |
| route_force_tunneling	 | Force tunneling route status |
| route_table_id | Route table ID |
| route_table_name | Route table name |

