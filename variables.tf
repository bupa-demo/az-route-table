variable "route_table_name" {
  description = "Route name"
  type        = string
}

variable "location" {
  description = "Azure location."
  type        = string
}

variable "environment" {
  description = "Project environment"
  type        = string
  default     = ""
}

variable "project" {
  description = "Project name"
  type        = string
  default     = ""
}

variable "resource_group_name" {
  description = "Resource group name"
  type        = string
}

variable "disable_bgp_route_propagation" {
  description = "Option to disable BGP route propagation on this Route Table."
  type        = bool
  default     = false
}

variable "enable_force_tunneling" {
  description = "Option to enable a route to Force Tunneling (force 0.0.0.0/0 traffic through the Gateway next hop)."
  type        = bool
  default     = false
}

variable "route_name" {
  type = list(object({
    name                    = string
    address_prefix          = string
    next_hop_type           = string
    next_hop_in_ip_address  = string
  }))
  default = []
}

variable "tags" {
  description = "Extra tags to add."
  type        = map(string)
  default     = {}
}
